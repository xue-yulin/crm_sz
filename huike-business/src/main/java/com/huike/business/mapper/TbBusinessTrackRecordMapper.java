package com.huike.business.mapper;

import java.util.List;
import com.huike.business.domain.TbBusinessTrackRecord;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * 商机跟进记录Mapper接口
 * @date 2021-04-28
 */
@Mapper
public interface TbBusinessTrackRecordMapper {

    /**
     * 新增商机跟进记录
     * @param tbBusinessTrackRecord
     * @return
     */
    int insertTbBusinessTrackRecord(TbBusinessTrackRecord tbBusinessTrackRecord);

    /**
     * id查询商机记录
     * @param id
     * @return
     */

    List<TbBusinessTrackRecord> selectTbBusinessTrackRecordListByBusinessId(Long id);
}