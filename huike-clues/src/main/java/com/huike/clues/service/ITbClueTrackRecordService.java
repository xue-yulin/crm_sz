package com.huike.clues.service;


import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.TbClueTrackRecord;
import com.huike.clues.domain.vo.ClueTrackRecordVo;
import com.huike.clues.service.impl.TbClueServiceImpl;
import com.huike.common.core.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 线索跟进记录Service接口
 *
 * @author WGL
 * @date 2022-04-19
 */

public interface ITbClueTrackRecordService {


    /**
     * 添加跟进记录
     * @param tbClueTrackRecordvo
     */
    void clue(ClueTrackRecordVo tbClueTrackRecordvo);

    /**
     *
     * @param clueId
     * @return
     */
    List<TbClueTrackRecord> list(Long clueId);
}
