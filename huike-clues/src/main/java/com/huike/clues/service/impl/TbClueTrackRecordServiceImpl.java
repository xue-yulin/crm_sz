package com.huike.clues.service.impl;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.TbClueTrackRecord;
import com.huike.clues.domain.vo.ClueTrackRecordVo;
import com.huike.clues.mapper.TbClueMapper;
import com.huike.clues.mapper.TbClueTrackRecordMapper;
import com.huike.clues.service.ITbClueService;
import com.huike.clues.service.ITbClueTrackRecordService;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.page.TableDataInfo;
import com.huike.common.utils.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 线索跟进记录Service业务层处理
 * @date 2022-04-22
 */
@Service
public class TbClueTrackRecordServiceImpl implements ITbClueTrackRecordService {

    @Autowired
    private TbClueMapper tbClueMapper ;

    @Autowired
    private TbClueTrackRecordMapper tbClueTrackRecordMapper;




    /**
     * 添加跟进记录
     * @param tbClueTrackRecordvo
     */
    @Override
    @Transactional
    public void clue(ClueTrackRecordVo tbClueTrackRecordvo) {
        TbClue tbClue = new TbClue();
        BeanUtils.copyProperties(tbClueTrackRecordvo,tbClue);
        tbClue.setId(tbClueTrackRecordvo.getClueId());
        tbClue.setCreateBy(SecurityUtils.getUsername());
        tbClue.setCreateTime(new Date());




        TbClueTrackRecord tbClueTrackRecord = new TbClueTrackRecord();
        //获取线索id
        tbClueTrackRecord.setClueId(tbClueTrackRecordvo.getClueId());

        BeanUtils.copyProperties(tbClueTrackRecordvo,tbClueTrackRecord);

        //新增
        tbClueTrackRecord.setCreateBy(SecurityUtils.getUsername());
        tbClueTrackRecord.setCreateTime(new Date());
        tbClueTrackRecordMapper.clue(tbClueTrackRecord);



    }

    /**
     * 查询线索跟进记录列表
     * @param clueId
     * @return
     */

    @Override
    public List<TbClueTrackRecord> list(Long clueId) {
        List<TbClueTrackRecord> list = tbClueTrackRecordMapper.list(clueId);
        return list;
    }


}
