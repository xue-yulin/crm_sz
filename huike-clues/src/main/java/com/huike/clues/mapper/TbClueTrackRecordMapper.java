package com.huike.clues.mapper;


import com.huike.clues.domain.TbClueTrackRecord;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 线索跟进记录Mapper接口
 *
 * @date 2021-04-19
 */
@Mapper
public interface TbClueTrackRecordMapper {


    /**
     * 增加跟进记录
     *
     * @param tbClueTrackRecord
     */
    @Insert("insert into tb_clue_track_record (clue_id, create_by, subject, record, level, create_time, type, false_reason, next_time) " +
            "values (#{clueId},#{createBy},#{subject},#{record},#{level},#{createTime},#{type},#{falseReason},#{nextTime})")
    void clue(TbClueTrackRecord tbClueTrackRecord);

    /**
     * 查询线索跟进记录列表
     *
     * @param clueId
     * @return

     */

    @Select("select * from tb_clue_track_record where clue_id =#{clueId}  order by create_time desc   ")
    List<TbClueTrackRecord> list(Long clueId);

    @Select("select count(*) from tb_clue_track_record where clue_id=#{clueId}")
    Long count(Long clueId);
}
