package com.huike.web.controller.report;


import com.huike.clues.domain.vo.ClueReportVo;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.page.TableDataInfo;
import com.huike.contract.domain.TbContract;
import com.huike.report.domain.vo.*;
import com.huike.report.service.IReportService;
import com.huike.task.service.RecoveryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import com.huike.report.domain.vo.ActivityStatisticsVo;
import com.huike.report.domain.vo.ChannelAndNumVO;
import com.huike.report.domain.vo.NameAndNumAndIdVO;
import com.huike.report.domain.vo.TbActivitys;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/report")
@Slf4j
public class ReportController extends BaseController {
    @Autowired
    private IReportService ireportService;
    /**
     *
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @GetMapping("/cluesStatistics/{beginCreateTime}/{endCreateTime}")
    public LineChartVO cluesStatistics(@PathVariable String beginCreateTime,@PathVariable String endCreateTime){
        LineChartVO lineChartVO = ireportService.cluesStatistics(beginCreateTime,endCreateTime);
        return lineChartVO;
    }

    /**
     * 线索统计
     * @param clueReportVo
     * @return
     */
    @GetMapping("/cluesStatisticsList")
    public TableDataInfo cluesStatisticsList(ClueReportVo clueReportVo){
        startPage();
        List<ClueReportVo> list= ireportService.cluesStatisticsList(clueReportVo);
        return getDataTable(list);
    }
    @Autowired
    private RecoveryService recoveryService;
    @Autowired
    private IReportService iReportService;

    /**
     * 线索转化率漏斗图
     *
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @GetMapping("/getVulnerabilityMap/{beginCreateTime}/{endCreateTime}")
    public AjaxResult tongji(@PathVariable("beginCreateTime") String beginCreateTime, @PathVariable("endCreateTime") String endCreateTime) {


        return AjaxResult.success(recoveryService.getVulnerabilityMap(beginCreateTime, endCreateTime));
    }

    /**
     * 渠道统计饼形图
     */

    @GetMapping("/chanelStatistics/{beginCreateTime}/{endCreateTime}")
    public List<ChannelAndNumVO> chanelStatistics(@PathVariable String beginCreateTime, @PathVariable String endCreateTime){

        log.info("接收参数：{}，{}",beginCreateTime,endCreateTime);

        return iReportService.chanelStatistics(beginCreateTime,endCreateTime);
    }


    /**
     * 渠道统计环形图
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @GetMapping("/activityStatistics/{beginCreateTime}/{endCreateTime}")
    public List<NameAndNumAndIdVO>  activityStatistics(@PathVariable String beginCreateTime, @PathVariable String endCreateTime){
        log.info("接收日期数据：{}，{}",beginCreateTime,endCreateTime);

        return iReportService.activityStatistics(beginCreateTime,endCreateTime);
    }


    /**
     * 渠道查询报表
     * @param tbActivitys
     * @return
     */
    @GetMapping("/activityStatisticsList")
    public TableDataInfo activityStatisticsList(TbActivitys tbActivitys){
        startPage();
        List<ActivityStatisticsVo> list = iReportService.activityStatisticsList(tbActivitys);
        return getDataTable(list);
    }

    @Autowired
    private IReportService reportService;

    /**
     * 客户统计列表查询
     *
     * @param contract
     * @return
     */
    @GetMapping("/contractStatisticsList")
    public TableDataInfo contractStatisticsList(TbContract contract) {
        //请求分页
        startPage();
        List<TbContract> tbContractList = reportService.contractStatisticsList(contract);
        //传入分页数据
        //getDataTable需要传的参数是一个集合
        TableDataInfo tableDataInfo = getDataTable(tbContractList);
        return tableDataInfo;
    }

    /**
     *  销售统计时间列表
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @GetMapping("/salesStatistics/{beginCreateTime}/{endCreateTime}")
    public LineChartVO salesStatistics(@PathVariable String beginCreateTime,
                                       @PathVariable String endCreateTime){
        log.info("接受参数:{}",beginCreateTime,endCreateTime);
        return iReportService.salesStatistics(beginCreateTime,endCreateTime);

    }


    /**
     * 归属部门
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @GetMapping("/deptStatisticsList/{beginCreateTime}/{endCreateTime}")
    public TableDataInfo deptStatisticsList(@PathVariable  String beginCreateTime,
                                            @PathVariable   String endCreateTime){
        log.info("接受参数:{}",beginCreateTime,endCreateTime);
        //设置分页数据
        startPage();

        List<Map<String, Object>> tableDataInfos=  iReportService.deptStatisticsList(beginCreateTime,endCreateTime);

        TableDataInfo tableDataInfo = getDataTable(tableDataInfos);

        return tableDataInfo;
    }

    /**
     * 归属人
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @GetMapping("/ownerShipStatisticsList/{beginCreateTime}/{endCreateTime}")
    public TableDataInfo ownerShipStatisticsList(@PathVariable  String beginCreateTime,
                                            @PathVariable   String endCreateTime){
        log.info("接受参数:{}",beginCreateTime,endCreateTime);
        startPage();

        List<Map<String, Object>> ownerShipStatisticsList = iReportService.ownerShipStatisticsList(beginCreateTime,endCreateTime);

        return getDataTablePage(ownerShipStatisticsList);
    }

    /**
     * 归属渠道
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @GetMapping("/channelStatisticsList/{beginCreateTime}/{endCreateTime}")
    public TableDataInfo channelStatisticsList(@PathVariable  String beginCreateTime,
                                                 @PathVariable   String endCreateTime){
        log.info("接受参数:{}",beginCreateTime,endCreateTime);
        startPage();

        List<Map<String, Object>> channelStatisticsList = iReportService.channelStatisticsList(beginCreateTime,endCreateTime);

        return getDataTablePage(channelStatisticsList);
    }





    /**
     * 客户统计折线
     *
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    //endCreateTime LineChartVO
    @GetMapping("/contractStatistics/{beginCreateTime}/{endCreateTime}")
    public LineChartVO contractStatistics(@PathVariable String beginCreateTime, @PathVariable String endCreateTime) {
        LineChartVO lineChartVO = reportService.contractStatistics(beginCreateTime, endCreateTime);
        return lineChartVO;
    }

    /**
     * 学科客户分布
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @GetMapping("/subjectStatistics/{beginCreateTime}/{endCreateTime}")
    public List subjectStatisticsList(@PathVariable String beginCreateTime, @PathVariable String endCreateTime) {
        List list=reportService.subject(beginCreateTime,endCreateTime);
        return list;
    }
}
