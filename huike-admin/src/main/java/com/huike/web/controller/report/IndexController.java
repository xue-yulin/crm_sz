package com.huike.web.controller.report;


import com.huike.common.core.domain.AjaxResult;
import com.huike.report.domain.vo.IndexVo;
import com.huike.report.service.IReportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/index")
@Slf4j
public class IndexController {

    @Autowired
    private IReportService reportService;

    /**
     * 首页统计
     * @param beginCreateTime
     * @param endCreateTime
     * @param deptId
     * @return
     */
    @GetMapping
    public AjaxResult homepageStatistic(String beginCreateTime,
                                        String endCreateTime,
                                        Long deptId ) {

        IndexVo indexVo = reportService.homepageStatistic(beginCreateTime, endCreateTime, deptId);

        return AjaxResult.success(indexVo);
    }

    /**
     * 线索转化龙虎榜
     *
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @GetMapping("/salesStatistic")
    public AjaxResult salesStatistic(String beginCreateTime,
                                     String endCreateTime,
                                     Long deptId){
        return AjaxResult.success(reportService.salesStatistic(beginCreateTime,endCreateTime,deptId));
    }

    /**
     * 商机转化龙虎榜接口
     * @return
     */
    @GetMapping("/businessChangeStatistics")
    public AjaxResult getBusinessChangeStatistics(String beginCreateTime,
                                                  String endCreateTime,
                                                  Long deptId){
        return AjaxResult.success(reportService.getBusinessChangeStatistics(beginCreateTime,endCreateTime));
    }
}