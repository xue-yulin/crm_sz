package com.huike.report.mapper;

import java.time.LocalDate;
import java.util.Map;
import java.util.List;
import java.util.Map;
import com.huike.report.domain.vo.SalesListVO;
import org.apache.ibatis.annotations.*;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.huike.clues.domain.vo.IndexStatisticsVo;
import com.huike.common.core.page.TableDataInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.huike.clues.domain.vo.IndexStatisticsVo;
import org.apache.ibatis.annotations.Select;
import java.time.LocalDateTime;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.huike.clues.domain.vo.IndexStatisticsVo;

/**
 * 首页统计分析的Mapper
 * @author Administrator
 *
 */
@Mapper
public interface ReportMapper {

    /**=========================================基本数据========================================*/

    /**
     * 统计时间列表
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    List<Map<String, Object>> salesStatistics(@Param("beginCreateTime") String beginCreateTime,@Param("endCreateTime") String endCreateTime);

    /**
     * 归属部门
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    List<Map<String, Object>> deptStatistics(@Param("beginCreateTime") String beginCreateTime,@Param("endCreateTime") String endCreateTime);

    /**
     * 归属人
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    List<Map<String, Object>> ownerShipStatistics(@Param("beginCreateTime") String beginCreateTime,@Param("endCreateTime") String endCreateTime);

    /**
     * 归属渠道
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    List<Map<String, Object>> channelStatistics(@Param("beginCreateTime") String beginCreateTime,@Param("endCreateTime") String endCreateTime);



    /**=========================================基本数据========================================*/

    @ResultMap("SalesListVOMap")
    List<SalesListVO> getSalesStatistics(@Param("beginTime") LocalDateTime beginTime, @Param("endTime") LocalDateTime endTime);


    @ResultMap("SalesListVOMap")
    List<SalesListVO> getBusinessSalesStatistics(@Param("beginTime") LocalDateTime beginTime, @Param("endTime") LocalDateTime endTime);



    /**=========================================基本数据========================================*/



	/**=========================================今日简报========================================*/



	/**=========================================待办========================================*/

}
