package com.huike.report.mapper;

import com.huike.report.domain.vo.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * 渠道统计分析的Mapper
 * @author Administrator
 *
 */
@Mapper
public interface ChannelMapper {

    /**
     * 渠道统计扇形图1
     * @param beginTime
     * @param endTime
     * @return
     */
    List<ChannelAndNumVO> chanelStatistics(@Param("beginTime") LocalDateTime beginTime, @Param("endTime") LocalDateTime endTime);

    /**
     * 渠道统计环形图
     * @param beginTime
     * @param endTime
     * @return
     */
    List<NameAndNumAndIdVO> activityStatistics(@Param("beginTime") LocalDateTime beginTime, @Param("endTime") LocalDateTime endTime);


    //查询字典
    List<sysDictDatavo> sysDictData();

    //查询渠道统计报表
    List<ActivityStatisticsVo> activityStatisticsList(TbActivitys tbActivitys);
}
