package com.huike.report.service.impl;
import com.huike.business.mapper.TbBusinessMapper;
import com.huike.clues.domain.vo.ClueReportVo;
import com.huike.clues.mapper.SysDeptMapper;
import com.huike.clues.mapper.SysDictDataMapper;
import com.huike.clues.mapper.TbClueMapper;
import com.huike.common.core.domain.entity.SysDept;
import com.huike.contract.domain.TbContract;
import com.huike.contract.mapper.TbContractMapper;
import com.huike.report.domain.vo.*;
import com.huike.report.mapper.ChannelMapper;
import com.huike.report.mapper.ReportMapper;
import com.huike.report.service.IReportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@Slf4j
public class ReportServiceImpl implements IReportService {
   @Autowired
    private ChannelMapper channelMapper;


@Autowired
    private ReportMapper reportMapper;
@Autowired
    private TbClueMapper tbClueMapper;

    @Autowired
    private TbClueMapper clueMapper;

    @Autowired
    private TbBusinessMapper businessMapper;

    @Autowired
    private TbContractMapper contractMapper;

    /**
     * *************看我看我**************
     * 传入两个时间范围，返回这两个时间范围内的所有时间，并保存在一个集合中
     *
     * @param beginTime
     * @param endTime
     * @return
     * @throws ParseException
     */
    public static List<String> findDates(String beginTime, String endTime)
            throws ParseException {
        List<String> allDate = new ArrayList();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Date dBegin = sdf.parse(beginTime);
        Date dEnd = sdf.parse(endTime);
        allDate.add(sdf.format(dBegin));
        Calendar calBegin = Calendar.getInstance();
        // 使用给定的 Date 设置此 Calendar 的时间
        calBegin.setTime(dBegin);
        Calendar calEnd = Calendar.getInstance();
        // 使用给定的 Date 设置此 Calendar 的时间
        calEnd.setTime(dEnd);
        // 测试此日期是否在指定日期之后
        while (dEnd.after(calBegin.getTime())) {

            // 根据日历的规则，为给定的日历字段添加或减去指定的时间量
            calBegin.add(Calendar.DAY_OF_MONTH, 1);
            allDate.add(sdf.format(calBegin.getTime()));
        }
        System.out.println("时间==" + allDate);
        return allDate;
    }


    /**
     * ************看我看我***********
     * 用我能少走很多路
     * 我是用来计算百分比的方法
      * @param all
     * 我是用来机选百分比的方法
     *
     * @param all
     * @param num
     * @return
     */
    private BigDecimal getRadio(Integer all, Long num) {
        if (all.intValue() == 0) {
            return new BigDecimal(0);
        }
        BigDecimal numBigDecimal = new BigDecimal(num);
        BigDecimal allBigDecimal = new BigDecimal(all);
        BigDecimal divide = numBigDecimal.divide(allBigDecimal, 4, BigDecimal.ROUND_HALF_UP);
        return divide.multiply(new BigDecimal(100));
    }

    /**
     *线索统计
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @Override
    public LineChartVO cluesStatistics(String beginCreateTime, String endCreateTime) {
        LineChartVO lineChartVO = new LineChartVO();
        //获取时间集合
        try {
            List<String> dates = findDates(beginCreateTime, endCreateTime);
            lineChartVO.setxAxis(dates);//设置时间
            List<LineSeriesVO> seriesVO = new ArrayList<>();
           List<Map<String,Object>> series = tbClueMapper.cluesStatistics(beginCreateTime,endCreateTime);
            LineSeriesVO lineSeriesVO1 = new LineSeriesVO();
            lineSeriesVO1.setName("新增线索数量");
            LineSeriesVO lineSeriesVO2 = new LineSeriesVO();
            lineSeriesVO2.setName("线索总数量");
            int sum = 0;
            for (String s : dates){
                Optional optional=  series.stream().filter(d->d.get("dd").equals(s)).findFirst();
                if(optional.isPresent()){
                    Map<String,Object> currentData=  (Map<String,Object>)optional.get();
                    lineSeriesVO1.getData().add(currentData.get("num"));
                    sum += Integer.parseInt(currentData.get("num").toString());
                }else{
                    lineSeriesVO1.getData().add(0);
                }
                lineSeriesVO2.getData().add(sum);
            }
            seriesVO.add(lineSeriesVO1);
            seriesVO.add(lineSeriesVO2);
            lineChartVO.setSeries(seriesVO);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return lineChartVO;
    }

    @Override
    public List<ClueReportVo> cluesStatisticsList(ClueReportVo clueReportVo) {
        List<ClueReportVo> clueList = tbClueMapper.cluesStatisticsList(clueReportVo);
        return clueList;
    }


    //渠道统计扇形图
    @Override
    public List<ChannelAndNumVO> chanelStatistics(String beginCreateTime, String endCreateTime) {


            DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            //开始时间
            LocalDate begin = LocalDate.parse(beginCreateTime,fmt);
            //结束时间
            LocalDate end = LocalDate.parse(endCreateTime,fmt);


            //吧开始时间和结束时间转换成LocalDateTime类型
            LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
            LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);

            //查询字典表
            List<sysDictDatavo> channel  = channelMapper.sysDictData();

            //调用mapper层
            List<ChannelAndNumVO> list  = channelMapper.chanelStatistics(beginTime,endTime);


        //遍历字典数据集合，判断渠道来源
        for (sysDictDatavo sysDictDatavo : channel) {
            for (ChannelAndNumVO channelAndNumVO : list) {
                if (sysDictDatavo.getDictValue().equals(channelAndNumVO.getChannel())) {
                    channelAndNumVO.setChannel(sysDictDatavo.getDictLabel());
                }
            }
        }
        return list;
    }

    /**
     * 渠道统计环形图
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @Override
    public List<NameAndNumAndIdVO> activityStatistics(String beginCreateTime, String endCreateTime) {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        //开始时间
        LocalDate begin = LocalDate.parse(beginCreateTime,fmt);
        //结束时间
        LocalDate end = LocalDate.parse(endCreateTime,fmt);


        //吧开始时间和结束时间转换成LocalDateTime类型
        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);

        List<NameAndNumAndIdVO> list = channelMapper.activityStatistics(beginTime,endTime);

        return list;
    }

    //查询渠道统计报表
    @Override
    public List<ActivityStatisticsVo> activityStatisticsList(TbActivitys tbActivitys) {

        //查询四张表 活动表  线索表  商机表 合同表 返回数据给一个集合
        List<ActivityStatisticsVo> list = channelMapper.activityStatisticsList(tbActivitys);

        return list;
    }


    /**
     * 销售统计时间列表
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @Override
    public LineChartVO salesStatistics(String beginCreateTime, String endCreateTime) {
        // 获取VO对象
        LineChartVO lineChartVo =new LineChartVO();
        try {
            // 调用方法，获取每天时间
            List<String> timeList= findDates(beginCreateTime,endCreateTime);
            // 把时间给X轴
            lineChartVo.setxAxis(timeList);

            List<LineSeriesVO> series = new ArrayList<>();
            //调用mapper返回集合
            List<Map<String,Object>>  statistics = reportMapper.salesStatistics(beginCreateTime,endCreateTime);

            LineSeriesVO lineSeriesVo=new LineSeriesVO();

            lineSeriesVo.setName("销售统计");

            for (String s : timeList) {
                // 获取返回的数据 进行判断
                Optional optional=  statistics.stream().filter(d->d.get("date").equals(s)).findFirst();
                if(optional.isPresent()){
                    Map<String,Object> cuurentData=  (Map<String,Object>)optional.get();
                    lineSeriesVo.getData().add(cuurentData.get("sales"));
                }else{
                    lineSeriesVo.getData().add(0);
                }

            }

            series.add(lineSeriesVo);
            lineChartVo.setSeries(series);

        } catch (ParseException e) {
             e.printStackTrace();
        }
        return  lineChartVo;

    }


    @Autowired
    private SysDeptMapper sysDeptMapper;

    /**
     * 归属部门
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @Override
    public List<Map<String,Object>> deptStatisticsList(String beginCreateTime, String endCreateTime) {

        List<Map<String,Object>> data= reportMapper.deptStatistics(beginCreateTime,endCreateTime);

        for (Map<String,Object> datum : data) {
            Long deptId= (Long) datum.get("dept_id");
            if(deptId!=null){
                SysDept dept= sysDeptMapper.selectDeptById(deptId);
                datum.put("deptName", dept.getDeptName());
            }
        }
        return data;

    }

    /**
     * 归属人
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @Override
    public List<Map<String, Object>> ownerShipStatisticsList(String beginCreateTime, String endCreateTime) {

        return  reportMapper.ownerShipStatistics(beginCreateTime,endCreateTime);

    }


    @Autowired
    private SysDictDataMapper sysDictDataMapper;

    /**
     * 客户统计列表查询
     *
     * @param contract
     * @return
     */
    @Override
    public List<TbContract> contractStatisticsList(TbContract contract) {
        List<TbContract> tbContractList = contractMapper.selectTbContractList(contract);
        return tbContractList;
    }
    /**
     * 归属渠道
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @Override
    public List<Map<String, Object>> channelStatisticsList(String beginCreateTime, String endCreateTime) {

        List<Map<String, Object>> data= reportMapper.channelStatistics(beginCreateTime,endCreateTime);


        for (Map<String, Object> datum : data) {
            String subjectValue= (String) datum.get("channel");
            if(subjectValue!=null){
                String lable=  sysDictDataMapper.selectDictLabel("clues_item",subjectValue);
                datum.put("channel",lable);
            }

        }


        return data;
    }




    /**
     * 客户统计时间列表
     *
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @Override
    public LineChartVO contractStatistics(String beginCreateTime, String endCreateTime) {
        //获取Vo对象
        LineChartVO lineChartVo = new LineChartVO();
        try {
            //调用findDates方法获得总共的天数
            List<String> allTimeList = findDates(beginCreateTime, endCreateTime);
            //将天数给X轴
            lineChartVo.setxAxis(allTimeList);

            List<LineSeriesVO> series = new ArrayList<>();
            //查询出每一天的人数
            List<Map<String, Object>> statistics = contractMapper.contractStatistics(beginCreateTime, endCreateTime);
//            两条折线（新增，总数）
//            新（集合接）
            LineSeriesVO lineSeries1 = new LineSeriesVO();
            lineSeries1.setName("新增客户数");
//            总（集合接）
            LineSeriesVO lineSeries2 = new LineSeriesVO();
            lineSeries2.setName("客户总数");

            //获取用户数
            int sum = 0;

            for (String s : allTimeList) {
                 //与数据库比对，取出第一个
                //filter()方法根据条件进行过滤，再调用 findFirst() 方法获取满足条件的第一个元素。
                //get(date)与s比较是否相等,只有满足条件的元素将被保留。
                //调用Stream流进行过滤，获取到statistics中的键为date的值（statistic全是有新增人数的日期），和allTimeList中的日期比较，找到第一个相同的。
                Optional optional = statistics.stream().filter(d -> d.get("date").equals(s)).findFirst();

                //optional.isPresent() 是 Optional 类中的一个方法，用于检查 Optional 对象中是否存在非null的值。
                if (optional.isPresent()) {
                     //不为空则相加
                    //获取到optionl中的值。将其转换为map类型
                    Map<String, Object> cuurentData = (Map<String, Object>) optional.get();
                    //添加到lineseries的date中。
                    lineSeries1.getData().add(cuurentData.get("num"));
                    //Integer.parseInt() 是 Java 中用于将字符串转换为整数类型的方法。
                    //current是一个map对象，用get获取到键为num的值
                    //通过 currentData.get("num") 可以获取到这个值，并通过 toString() 将其转换为字符串类型。
                    //接着，使用 Integer.parseInt() 将字符串转换为整数类型
                    sum += Integer.parseInt(cuurentData.get("num").toString());
                } else {
//                    为0新增数就为0
                    lineSeries1.getData().add(0);
                }
                lineSeries2.getData().add(sum);
            }
            series.add(lineSeries1);
            series.add(lineSeries2);
            lineChartVo.setSeries(series);
        } catch (ParseException e) {
            // e.printStackTrace();
        }
        return lineChartVo;
    }

    /**
     * 学科客户分布(饼状图)
     *
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    //  增强for  for(元素的数据类型 变量名 : 数组或者集合)
    //涉及两张表，合同表和字典数据表
    @Override
    public List<Map<String, Object>> subject(String beginCreateTime, String endCreateTime) {
        //查询各个不同的学科的一周内的人数（分组查询）
        //查询各个学科和学科对应的人数
        List<Map<String, Object>> data = contractMapper.subject(beginCreateTime, endCreateTime,4);

        for (Map<String, Object> map : data) {
            //根据subject键获取相对应的值(课程编号)。
            String value = (String) map.get("subject");
            //查询字典数据表，根据字典类型（为course_subject）和value（课程编号）查询到相对应的课程名称
            String zhi = sysDictDataMapper.selectDictLabel("course_subject", value);
            //为subject键赋值
            map.put("subject", zhi);
        }
        return data;
    }
    /**
     * 线索转化龙虎榜
     *
     * @param beginCreateTime
     * @param endCreateTime
     * @param deptId
     * @return
     */
    @Override
    public List<SalesListVO> salesStatistic(String beginCreateTime, String endCreateTime, Long deptId) {
        //创建日期解析对象
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                                                //将String类型的日期按照formatter格式解析为localdate对象
        LocalDateTime beginTime = LocalDateTime.of(LocalDate.parse(beginCreateTime, formatter), LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(LocalDate.parse(endCreateTime, formatter), LocalTime.MAX);

        //查出每个用户的成功转化的线索条数
        List<SalesListVO> salesListVO = reportMapper.getSalesStatistics(beginTime, endTime);

        for (SalesListVO listVO : salesListVO) {
            //总线索条数
            Integer totalCluesNumber = clueMapper.getAllCluesByUserId(beginTime, endTime);
            Long num = Long.valueOf(listVO.getNum());
            BigDecimal radio = getRadio(totalCluesNumber, num);
            listVO.setRadio(radio.setScale(2, RoundingMode.HALF_DOWN));
        }

        return salesListVO;
    }

    /**
     * 商机转化龙虎榜
     *
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @Override
    public List<SalesListVO> getBusinessChangeStatistics(String beginCreateTime, String endCreateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime beginTime = LocalDateTime.of(LocalDate.parse(beginCreateTime, formatter), LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(LocalDate.parse(endCreateTime, formatter), LocalTime.MAX);
        //每个用户转换成功的商机数
        List<SalesListVO> salesListVO = reportMapper.getBusinessSalesStatistics(beginTime, endTime);

        for (SalesListVO listVO : salesListVO) {
            //所有商机数
            Integer totalBusinessNumber = businessMapper.getAllBusinessByUserId(beginTime, endTime);
            if (totalBusinessNumber != null) {
                Long num = Long.valueOf(listVO.getNum());
                BigDecimal radio = getRadio(totalBusinessNumber, num);
                listVO.setRadio(radio.setScale(2, RoundingMode.HALF_DOWN));
            }
        }

        return salesListVO;
    }

    /**
     * 首页统计
     *
     * @param beginCreateTime
     * @param endCreateTime
     * @param deptId
     * @return
     */
    @Override
    public IndexVo homepageStatistic(String beginCreateTime, String endCreateTime, Long deptId) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime beginTime = LocalDateTime.of(LocalDate.parse(beginCreateTime, formatter), LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(LocalDate.parse(endCreateTime, formatter), LocalTime.MAX);

        LocalDateTime today = LocalDateTime.of(LocalDate.now(), LocalTime.MIN);
        //线索数目
        Integer cluesNum = clueMapper.getAllCluesNumber(beginTime, endTime);

        //今日线索数目
        Integer todayCluesNum = clueMapper.getTodayClueNumber(today);

        //待跟进线索数目
        Integer status = 1;
        Integer toFollowedCluesNum = clueMapper.getTofollowedCluesNum(beginTime, endTime, status);

        //待分配线索数目
        Integer toallocatedCluesNum = clueMapper.getToallocatedCluesNum(beginTime, endTime);

        //商机数目
        Integer businessNum = businessMapper.getAllBusinessesNumber(beginTime, endTime);


        //今日商机数目
        Integer todayBusinessNum = businessMapper.getTodayBusinessNumber(today);

        //待跟进商机数目
        Integer tofollowedBusinessNum = businessMapper.getTofollowedBusinessNum(beginTime, endTime, status);


        //待分配商机数目
        Integer toallocatedBusinessNum = businessMapper.getToallocatedBusinessNum(beginTime, endTime);

        //合同数目
        Integer contractNum = contractMapper.getAllContractsNumber(beginTime, endTime);

        //今日合同数目
        Integer todayContractNum = contractMapper.getTodayContractNumber(today);

        //销售金额
        Double salesAmount = contractMapper.getSalesAmount(beginTime, endTime);

        //今日销售金额
        Double todaySalesAmount = contractMapper.getTodaySalesAmount(today);

        return IndexVo
                .builder()
                .cluesNum(cluesNum)
                .todayCluesNum(todayCluesNum)
                .tofollowedCluesNum(toFollowedCluesNum)
                .toallocatedCluesNum(toallocatedCluesNum)
                .businessNum(businessNum)
                .todayBusinessNum(todayBusinessNum)
                .tofollowedBusinessNum(tofollowedBusinessNum)
                .toallocatedBusinessNum(toallocatedBusinessNum)
                .contractNum(contractNum)
                .todayContractNum(todayContractNum)
                .salesAmount(salesAmount)
                .todaySalesAmount(todaySalesAmount)
                .build();
    }
}




