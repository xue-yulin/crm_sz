package com.huike.report.service;

import java.text.ParseException;
import com.huike.contract.domain.TbContract;
import java.time.LocalDate;
import com.huike.report.domain.vo.IndexVo;
import com.huike.report.domain.vo.LineChartVO;
import com.huike.report.domain.vo.SalesListVO;

import java.util.List;
import java.util.Map;
import java.util.Map;

import com.huike.common.core.page.TableDataInfo;
import com.huike.report.domain.vo.*;
import org.apache.ibatis.annotations.Param;

import com.huike.clues.domain.TbActivity;
import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.vo.IndexStatisticsVo;
import com.huike.contract.domain.TbContract;

import com.huike.report.domain.vo.*;

import com.huike.clues.domain.vo.ClueReportVo;
import com.huike.report.domain.vo.*;
import org.apache.ibatis.annotations.Param;

import com.huike.clues.domain.TbActivity;
import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.vo.IndexStatisticsVo;
import com.huike.contract.domain.TbContract;
import java.util.Map;

public interface IReportService {
   /* *//**
     * 漏斗
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     *//*
    VulnerabilityMapVo tongji(LocalDate beginCreateTime, LocalDate endCreateTime);*/

    List<TbContract> contractStatisticsList(TbContract contract);

    LineChartVO contractStatistics(String beginCreateTime, String endCreateTime);


    /**
     * 学科客户分布
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    List<Map<String, Object>> subject(String beginCreateTime, String endCreateTime);

    LineChartVO cluesStatistics(String beginCreateTime, String endCreateTime);


    List<ClueReportVo> cluesStatisticsList(ClueReportVo clueReportVo);
    List<SalesListVO> salesStatistic(String beginCreateTime, String endCreateTime, Long deptId);

    List<SalesListVO> getBusinessChangeStatistics(String beginCreateTime, String endCreateTime);

    IndexVo homepageStatistic(String beginCreateTime, String endCreateTime, Long deptId);
    //渠道统计扇形图
    List<ChannelAndNumVO> chanelStatistics(String beginCreateTime, String endCreateTime);


    //渠道统计环形图
    List<NameAndNumAndIdVO> activityStatistics(String beginCreateTime, String endCreateTime);

    //查询渠道统计报表
    List<ActivityStatisticsVo> activityStatisticsList(TbActivitys tbActivitys);

    /**
     * 销售统计时间列表
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    LineChartVO salesStatistics(String beginCreateTime, String endCreateTime);

    /**
     *归属部门
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    List<Map<String, Object>> deptStatisticsList(String beginCreateTime, String endCreateTime);


    /**
     * 归属人
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    List<Map<String, Object>> ownerShipStatisticsList(String beginCreateTime, String endCreateTime);

    /**
     * 归属渠道
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    List<Map<String, Object>> channelStatisticsList(String beginCreateTime, String endCreateTime);
}
