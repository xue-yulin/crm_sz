package com.huike.report.domain.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author 薛坤
 * @version 1.0
 */

@Data
public class SalesListVO {
    private String create_by;
    private String deptName;
    private Integer num;
    private Long dep_id;
    private BigDecimal radio;
}
