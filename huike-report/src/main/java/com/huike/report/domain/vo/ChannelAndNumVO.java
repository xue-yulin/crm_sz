package com.huike.report.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//渠道统计饼形图实体对象
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChannelAndNumVO {
    private String channel; //渠道
    private Integer num;//数量

}
