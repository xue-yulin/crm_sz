package com.huike.report.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//渠道统计环形图实体对象
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NameAndNumAndIdVO {
    private String activity; //活动名称
    private Integer num;//数量
    private Long activityId;//活动id

}
