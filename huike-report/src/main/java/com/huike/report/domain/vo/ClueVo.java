package com.huike.report.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClueVo {
    private Long id;//活动id
    private String channel; //渠道
    private Integer cluesNum=0;  //线索总数
    private Integer falseCluesNum=0;  //伪线索数目
    private Integer businessNum=0;  //转换商机数目
    private Integer customersNum=0;  //转换客户数目
    private Double amount; //成交金额
    private Long activityId;//活动外键id
    private String code; //活动编码
}
