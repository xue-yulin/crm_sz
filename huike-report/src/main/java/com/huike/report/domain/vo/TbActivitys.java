package com.huike.report.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.huike.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TbActivitys {
    private static final long serialVersionUID = 1L;

    //编码
    private String code;

    private String name;

    /** 渠道来源 */
    @Excel(name = "渠道来源")
    private String channel;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date beginTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date endTime;
}
