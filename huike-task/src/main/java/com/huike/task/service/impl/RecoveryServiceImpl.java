package com.huike.task.service.impl;

import com.huike.business.mapper.TbBusinessMapper;
import com.huike.clues.mapper.TbClueMapper;
import com.huike.contract.mapper.TbContractMapper;
import com.huike.report.domain.vo.VulnerabilityMapVo;
import com.huike.task.mapper.RecoveryMapper;
import com.huike.task.service.RecoveryService;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class RecoveryServiceImpl implements RecoveryService {

    @Autowired
    private RecoveryMapper mapper;
    @Autowired
    private TbClueMapper clueMapper;
    @Autowired
    private TbBusinessMapper businessMapper;
    @Autowired
    private TbContractMapper contractMapper;
    private String Tbclue_RECOVERY = "3";

    private String TbBusiness_RECOVERY = "3";


    @Override
    public void recoveryBusiness() {
        mapper.resetNextTimeAndStatusOnBusiness(TbBusiness_RECOVERY, new Date());
    }

    @Override
    public void recoveryClue() {
        //回收线索
        mapper.resetNextTimeAndStatusOnClue(Tbclue_RECOVERY, new Date());
    }

    /**
     * 线程转化率漏斗图
     *
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @Override
    public VulnerabilityMapVo getVulnerabilityMap(String beginCreateTime, String endCreateTime) {
        VulnerabilityMapVo mapvo = new VulnerabilityMapVo();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime beginTime = LocalDateTime.of(LocalDate.parse(beginCreateTime, formatter), LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(LocalDate.parse(endCreateTime, formatter), LocalTime.MAX);
        //线索总数
        int cluesNums = clueMapper.getCluesNums(beginTime, endTime);
        System.err.println("线索总数" + cluesNums);
        //有效线索数
        int effectiveCluesNums = clueMapper.getEffectiveCluesNums(beginTime, endTime);
        System.err.println("有效线索数" + effectiveCluesNums);
        //商机数量
        int businessNums = businessMapper.getBusinessNums(beginTime, endTime);
        System.err.println("商机数量" + businessNums);
        //合同数量
        int contractNums = contractMapper.getContracyNums(beginTime, endTime);
        System.err.println("商机数量" + contractNums);
        mapvo.setCluesNums(cluesNums);
        mapvo.setEffectiveCluesNums(effectiveCluesNums);
        mapvo.setBusinessNums(businessNums);
        mapvo.setContractNums(contractNums);
        return mapvo;

    }
}
