package com.huike.task.service;

import com.huike.report.domain.vo.VulnerabilityMapVo;

import java.time.LocalDate;

public interface RecoveryService {

    void recoveryBusiness();

    void recoveryClue();

    VulnerabilityMapVo getVulnerabilityMap(String beginCreateTime, String endCreateTime);
}
